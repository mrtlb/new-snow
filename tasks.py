import common_tasks
import os
from invoke import Collection, task


@task
def api_key(ctx):
    common_tasks.vault_login(ctx)
    print("\n\nApiKey = ", end="")
    ctx.run("vault kv get -address='https://vault.services.graingercloud.com' -field apikey secret/keepstock/shared/snow").stdout

