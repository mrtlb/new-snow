package com.grainger.keepstock.newsnow.services

import com.grainger.keepstock.newsnow.models.CreateAppRequest
import com.grainger.keepstock.newsnow.models.CreateTaskRequest
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import tornadofx.Component
import tornadofx.Rest
import tornadofx.information
import java.io.StringReader
import java.io.StringWriter
import java.util.prefs.Preferences
import javax.json.Json

@Service
class RequestService : Component() {
    companion object {
        val CREATE_APP_KEY = "createAppRequest"
        val CREATE_TASKS_KEY = "createTasksRequest"
    }

    @Value("classpath:data/defaultAppRequest.json")
    private var defaultAppRequest: Resource? = null

    @Value("classpath:data/defaultTaskRequests.json")
    private var defaultTaskRequests: Resource? = null

    @Value("\${app.snow.base-url}")
    private var snowBaseUrl: String? = null

    @Value("\${app.snow.create-request-url}")
    private var snowCreateRequestUrl: String? = null

    @Value("\${app.snow.create-task-url}")
    private var snowCreateTasksUrl: String? = null

    private val api: Rest by inject()

    init {
        api.engine.requestInterceptor = {
            println("${it.method} request to ${it.uri}\nBody: ${it.entity}")
        }
        api.engine.responseInterceptor = {
            println("Response: ${it.statusCode}-${it.status}\nHeaders: ${it.headers}\nBody: ${it.text()}")
        }
    }

    private val prefs: Preferences
        get() {
            return Preferences.userRoot().node("new-snow")
        }

    val appRequest: CreateAppRequest
        get() {
            val model = CreateAppRequest()
            if (prefs.get(CREATE_APP_KEY, null).isNullOrEmpty()) {
                resetDefaultAppRequest(model)
            } else {
                loadAppRequest(model)
            }
            return model
        }

    val taskRequests: List<CreateTaskRequest>
        get() {
            return if (prefs.get(CREATE_APP_KEY, null).isNullOrEmpty()) {
                resetDefaultTaskRequests()
            } else {
                loadTaskRequests()
            }
        }

    fun clearAllPrefs() = prefs.clear()

    fun saveTaskRequests(tasks: List<CreateTaskRequest>) {
        with(prefs) {
            val writer = StringWriter()
            val array = with(Json.createArrayBuilder()) {
                tasks.map { add(it.toJSON()) }
                build()
            }
            Json.createWriter(writer).writeArray(array)
            put(CREATE_TASKS_KEY, writer.buffer.toString())
        }
        prefs.flush()
    }

    fun saveAppRequest(item: CreateAppRequest) {
        with(prefs) {
            val writer = StringWriter()
            Json.createWriter(writer).write(item.toJSON())
            put(CREATE_APP_KEY, writer.buffer.toString())
        }
        prefs.flush()
    }

    fun loadTaskRequests(): MutableList<CreateTaskRequest> {
        val tasks = mutableListOf<CreateTaskRequest>()
        with(prefs) {
            val reader = StringReader(get(CREATE_TASKS_KEY, "[]"))
            val array = Json.createReader(reader).readArray()
            tasks.clear()
            array.map {
                val r = CreateTaskRequest()
                r.updateModel(it.asJsonObject())
                tasks.add(r)
            }
        }
        return tasks
    }

    fun loadAppRequest(model: CreateAppRequest) {
        with(prefs) {
            val reader = StringReader(get(CREATE_APP_KEY, "{}"))
            model.updateModel(Json.createReader(reader).readObject())
        }
    }

    fun resetDefaultAppRequest(model: CreateAppRequest) {
        prefs.remove(CREATE_APP_KEY)
        val reader = Json.createReader(defaultAppRequest!!.inputStream)
        model.updateModel(reader.readObject())
    }

    fun resetDefaultTaskRequests(): MutableList<CreateTaskRequest> {
        val tasks = mutableListOf<CreateTaskRequest>()

        prefs.remove(CREATE_TASKS_KEY)
        val reader = Json.createReader(defaultTaskRequests!!.inputStream)
        val array = reader.readArray()
        tasks.clear()
        array.map {
            val r = CreateTaskRequest()
            r.updateModel(it.asJsonObject())
            tasks.add(r)
        }
        return tasks
    }

    data class CreateResponse(val sysid: String, val changeNumber: String?)

    fun postAppRequest(apiKey: String?, request: CreateAppRequest, messages: (String) -> Unit = {}): CreateResponse {
        api.baseURI = snowBaseUrl
        var response = api.post(snowCreateRequestUrl!!, request) { request ->
            request.addHeader("x-api-key", apiKey!!)
        }
        var sysid: String? = null
        var changeNumber: String? = null

        if (response.ok()) {
            val one = response.one()
            sysid = one.getString("change_sys_id")
            changeNumber = one.getString("change_number")

            val msg = "A create request was submitted with a response of ${response.text()}"
            messages("Create App Success $msg")
            messages("New SNOW Ticket ID = $sysid")
            messages("New Change Request = $changeNumber")
        } else {
            val msg = "A create request failed with status ${response.status} and message ${response.text()}"
            messages("Create App Failed: $msg")
            throw Exception(msg)
        }
        return CreateResponse(sysid, changeNumber)
    }

    fun postTasksRequest(apiKey: String?, change_sys_id: String, requests: List<CreateTaskRequest>, messages: (String) -> Unit = {}) {
        api.baseURI = snowBaseUrl
        val reqObj = Json.createObjectBuilder()
        requests.forEachIndexed { idx, task ->
            task.change_sys = change_sys_id
            reqObj.add("Task0$idx", task.toJSON())
        }

        var response = api.post(snowCreateTasksUrl!!, reqObj.build()) { request ->
            request.addHeader("x-api-key", apiKey!!)
        }

        if (response.ok()) {
            val msg = "A create tasks request was submitted with a response of ${response.text()}"
            messages("Create Tasks Success: $msg")
        } else {
            val msg = "A create tasks request failed with status ${response.status} and message ${response.text()}"
            messages("Create Tasks Failed: $msg")
        }
    }
}
