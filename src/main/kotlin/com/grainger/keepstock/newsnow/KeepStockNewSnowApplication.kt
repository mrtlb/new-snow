package com.grainger.keepstock.newsnow

import com.grainger.keepstock.newsnow.views.MainView
import com.grainger.keepstock.newsnow.views.Styles
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.ConfigurableApplicationContext
import tornadofx.App
import tornadofx.DIContainer
import tornadofx.FX
import tornadofx.launch
import kotlin.reflect.KClass

@SpringBootApplication
class KeepStockNewSnowApplication : App(MainView::class, Styles::class) {

    private lateinit var context: ConfigurableApplicationContext

    override fun init() {
        this.context = runApplication<KeepStockNewSnowApplication>(*saveArgs)
        context.autowireCapableBeanFactory.autowireBean(this)

        FX.dicontainer = object : DIContainer {
            override fun <T : Any> getInstance(type: KClass<T>): T = context.getBean(type.java)
            override fun <T : Any> getInstance(type: KClass<T>, name: String): T = context.getBean(name, type.java)
        }
    }

    override fun stop() { // On stop, we have to stop spring as well
        super.stop()
        context.close()
    }
}

private lateinit var saveArgs: Array<String>

fun main(args: Array<String>) {
    saveArgs = args
    launch<KeepStockNewSnowApplication>(args)
}

