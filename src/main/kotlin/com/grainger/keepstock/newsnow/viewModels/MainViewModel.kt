package com.grainger.keepstock.newsnow.viewModels

import com.grainger.keepstock.newsnow.models.CreateAppRequest
import com.grainger.keepstock.newsnow.services.RequestService
import javafx.beans.property.SimpleListProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import tornadofx.ItemViewModel
import tornadofx.information
import tornadofx.plusAssign

class MainViewModel(createAppRequest: CreateAppRequest? = null) :
    ItemViewModel<CreateAppRequest>(createAppRequest) {

    val requestService: RequestService by di()

    init {
        if (isEmpty) item = requestService.appRequest
    }
    var tasks = bind {
        SimpleListProperty(
            this, "tasks",
            FXCollections.observableList(
                requestService.taskRequests.map { TaskViewModel(it) }.toMutableList()
            )
        )
    }
    var apiKey = bind {
        SimpleStringProperty(this, "apiKey", config.string("apiKey"))
    }
    var statusMessages = bind {
        SimpleStringProperty(this, "statusMessages", "")
    }

    val entered_by = bind(CreateAppRequest::entered_byProperty)
    val source_of_change = bind(CreateAppRequest::source_of_changeProperty)
    val configuration_item = bind(CreateAppRequest::configuration_itemProperty)
    val standard_change = bind(CreateAppRequest::standard_changeProperty)
    val short_description = bind(CreateAppRequest::short_descriptionProperty)
    val description = bind(CreateAppRequest::descriptionProperty)
    val remediation_failure_plan = bind(CreateAppRequest::remediation_failure_planProperty)
    val test_plan = bind(CreateAppRequest::test_planProperty)
    val pretest_plan = bind(CreateAppRequest::pretest_planProperty)
    val posttest_plan = bind(CreateAppRequest::posttest_planProperty)
    val implementation_plan = bind(CreateAppRequest::implementation_planProperty)
    val reason_for_change = bind(CreateAppRequest::reason_for_changeProperty)
    val planned_start_date = bind(CreateAppRequest::planned_start_dateProperty)
    val planned_end_date = bind(CreateAppRequest::planned_end_dateProperty)
    val contact = bind(CreateAppRequest::contactProperty)
    val contact_phone = bind(CreateAppRequest::contact_phoneProperty)
    val country = bind(CreateAppRequest::countryProperty)
    val production_target_system = bind(CreateAppRequest::production_target_systemProperty)

    val canSubmit =
        entered_by.isNotEmpty.and(
            source_of_change.isNotEmpty.and(
                configuration_item.isNotEmpty.and(
                    standard_change.isNotEmpty.and(
                        short_description.isNotEmpty.and(
                            description.isNotEmpty.and(
                                remediation_failure_plan.isNotEmpty.and(
                                    test_plan.isNotEmpty.and(
                                        pretest_plan.isNotEmpty.and(
                                            posttest_plan.isNotEmpty.and(
                                                implementation_plan.isNotEmpty.and(
                                                    reason_for_change.isNotEmpty.and(
                                                        planned_start_date.isNotEmpty.and(
                                                            planned_end_date.isNotEmpty.and(
                                                                contact.isNotEmpty.and(
                                                                    contact_phone.isNotEmpty.and(
                                                                        country.isNotEmpty.and(
                                                                            production_target_system.isNotEmpty
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        ).run {
            tasks.get().fold(this) { acc, task -> acc.and(task.canSubmit) }
        }

    fun submit() {
        this.commit()

        try {
            val (sysid, changeNumber) = requestService.postAppRequest(apiKey.get(), item) {
                statusMessages.plusAssign(it + "\n")
            }
            if (!sysid.isNullOrEmpty()) {
                requestService.postTasksRequest(apiKey.get(), sysid, tasks.map { it.item }) {
                    statusMessages.plusAssign(it + "\n")
                }
            }
            information("Success Creating Request", "Change $changeNumber was succesfully created.")
        } catch (e: Exception) {
            tornadofx.error("Error Creating Request", e.message)
        }
    }

    override fun onCommit() {
        super.onCommit()
        // TODO Post REST here
        requestService.saveTaskRequests(
            tasks.get().map {
                it.commit()
                it.item
            }
        )
        if (apiKey.get() != null) {
            with(config) {
                put("apiKey", apiKey.get())
                save()
            }
        }
        requestService.saveAppRequest(item)
    }

    fun invalidate() {
        requestService.resetDefaultAppRequest(requestService.appRequest)
        item = requestService.appRequest
    }
}
