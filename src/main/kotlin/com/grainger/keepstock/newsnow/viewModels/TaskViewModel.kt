package com.grainger.keepstock.newsnow.viewModels

import com.grainger.keepstock.newsnow.models.CreateTaskRequest
import com.grainger.keepstock.newsnow.services.RequestService
import tornadofx.ItemViewModel

class TaskViewModel(createTaskRequest: CreateTaskRequest = CreateTaskRequest()) :
    ItemViewModel<CreateTaskRequest>(createTaskRequest) {
    val requestService: RequestService by di()

    val assignment_group = bind(CreateTaskRequest::assignment_groupProperty)
    val change_sys = bind(CreateTaskRequest::change_sysProperty)
    val description = bind(CreateTaskRequest::descriptionProperty)
    val short_description = bind(CreateTaskRequest::short_descriptionProperty)
    val start = bind(CreateTaskRequest::startProperty)
    val state = bind(CreateTaskRequest::stateProperty)

    val canSubmit = assignment_group.isNotEmpty.and(
        change_sys.isNotEmpty.and(
            description.isNotEmpty.and(
                short_description.isNotEmpty.and(
                    start.isNotEmpty.and(
                        state.isNotEmpty
                    )
                )
            )
        )
    )
}
