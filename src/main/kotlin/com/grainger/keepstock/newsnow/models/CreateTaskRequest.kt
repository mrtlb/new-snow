package com.grainger.keepstock.newsnow.models

import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.JsonModel
import javax.json.JsonObject

class CreateTaskRequest(
    assignment_group: String? = null,
    change_sys: String? = null,
    description: String? = null,
    short_description: String? = null,
    start: String? = null,
    state: String? = null
) : JsonModel {
    val assignment_groupProperty = SimpleStringProperty(this, "assignment_group", assignment_group)
    var assignment_group by assignment_groupProperty

    val change_sysProperty = SimpleStringProperty(this, "change_sys", change_sys)
    var change_sys by change_sysProperty

    val descriptionProperty = SimpleStringProperty(this, "description", description)
    var description by descriptionProperty

    val short_descriptionProperty = SimpleStringProperty(this, "short_description", short_description)
    var short_description by short_descriptionProperty

    val startProperty = SimpleStringProperty(this, "start", start)
    var start by startProperty

    val stateProperty = SimpleStringProperty(this, "state", state)
    var state by stateProperty

    override fun updateModel(json: JsonObject) {
        with(json) {
            assignment_group = string("assignment_group")
            change_sys = string("change_sys")
            description = string("description")
            short_description = string("short_description")
            start = string("start")
            state = string("state")
        }
    }

    override fun toJSON(json: JsonBuilder) {
        with(json) {
            add("assignment_group", assignment_group)
            add("change_sys", change_sys)
            add("description", description)
            add("short_description", short_description)
            add("start", start)
            add("state", state)
        }
    }
}
