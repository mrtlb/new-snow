package com.grainger.keepstock.newsnow.models

import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import javax.json.JsonObject

class CreateAppRequest(
    entered_by: String? = null,
    source_of_change: String? = null,
    configuration_item: String? = null,
    standard_change: String? = null,
    short_description: String? = null,
    description: String? = null,
    remediation_failure_plan: String? = null,
    test_plan: String? = null,
    pretest_plan: String? = null,
    posttest_plan: String? = null,
    implementation_plan: String? = null,
    reason_for_change: String? = null,
    planned_start_date: String? = null,
    planned_end_date: String? = null,
    contact: String? = null,
    contact_phone: String? = null,
    country: String? = null,
    production_target_system: String? = null
) : JsonModel {
    val entered_byProperty = SimpleStringProperty(this, "entered_by", entered_by)
    var entered_by by entered_byProperty

    val source_of_changeProperty = SimpleStringProperty(this, "source_of_change", source_of_change)
    var source_of_change by source_of_changeProperty

    val configuration_itemProperty = SimpleStringProperty(this, "configuration_item", configuration_item)
    var configuration_item by configuration_itemProperty

    val standard_changeProperty = SimpleStringProperty(this, "standard_change_property", standard_change)
    var standard_change by standard_changeProperty

    val short_descriptionProperty = SimpleStringProperty(this, "short_description", short_description)
    var short_description by short_descriptionProperty

    val descriptionProperty = SimpleStringProperty(this, "description", description)
    var description by descriptionProperty

    val remediation_failure_planProperty = SimpleStringProperty(this, "remediation_failure_plan", remediation_failure_plan)
    var remediation_failure_plan by remediation_failure_planProperty

    val test_planProperty = SimpleStringProperty(this, "test_plan", test_plan)
    var test_plan by test_planProperty

    val pretest_planProperty = SimpleStringProperty(this, "pretest_plan", pretest_plan)
    var pretest_plan by pretest_planProperty

    val posttest_planProperty = SimpleStringProperty(this, "posttest_plan", posttest_plan)
    var posttest_plan by posttest_planProperty

    val implementation_planProperty = SimpleStringProperty(this, "implementation_plan", implementation_plan)
    var implementation_plan by implementation_planProperty

    val reason_for_changeProperty = SimpleStringProperty(this, "reason_for_change", reason_for_change)
    var reason_for_change by reason_for_changeProperty

    val planned_start_dateProperty = SimpleStringProperty(this, "planned_start_date", planned_start_date)
    var planned_start_date by planned_start_dateProperty

    val planned_end_dateProperty = SimpleStringProperty(this, "planned_end_date", planned_end_date)
    var planned_end_date by planned_end_dateProperty

    val contactProperty = SimpleStringProperty(this, "contact", contact)
    var contact by contactProperty

    val contact_phoneProperty = SimpleStringProperty(this, "contact_phone", contact_phone)
    var contact_phone by contact_phoneProperty

    val countryProperty = SimpleStringProperty(this, "country", country)
    var country by countryProperty

    val production_target_systemProperty = SimpleStringProperty(this, "production_target_system", production_target_system)
    var production_target_system by production_target_systemProperty

    override fun updateModel(json: JsonObject) {
        with(json) {
            entered_by = string("entered_by")
            source_of_change = string("source_of_change")
            configuration_item = string("configuration_item")
            standard_change = string("standard_change")
            short_description = string("short_description")
            description = string("description")
            remediation_failure_plan = string("remediation_failure_plan")
            test_plan = string("test_plan")
            pretest_plan = string("pretest_plan")
            posttest_plan = string("posttest_plan")
            implementation_plan = string("implementation_plan")
            reason_for_change = string("reason_for_change")
            planned_start_date = string("planned_start_date")
            planned_end_date = string("planned_end_date")
            contact = string("contact")
            contact_phone = string("contact_phone")
            country = string("country")
            production_target_system = string("production_target_system")
        }
    }

    override fun toJSON(json: JsonBuilder) {
        with(json) {
            add("entered_by", entered_by)
            add("source_of_change", source_of_change)
            add("configuration_item", configuration_item)
            add("standard_change", standard_change)
            add("short_description", short_description)
            add("description", description)
            add("remediation_failure_plan", remediation_failure_plan)
            add("test_plan", test_plan)
            add("pretest_plan", pretest_plan)
            add("posttest_plan", posttest_plan)
            add("implementation_plan", implementation_plan)
            add("reason_for_change", reason_for_change)
            add("planned_start_date", planned_start_date)
            add("planned_end_date", planned_end_date)
            add("contact", contact)
            add("contact_phone", contact_phone)
            add("country", country)
            add("production_target_system", production_target_system)
        }
    }
}
