package com.grainger.keepstock.newsnow.views

import javafx.scene.paint.Color
import javafx.scene.text.FontWeight
import tornadofx.*

class Styles : Stylesheet() {
    companion object {
        val titleClass by cssclass()
    }
    init {
        fieldset and titleClass child label {
            fontSize = 2.em
            textFill = Color.BLUE
        }

        textArea {
            minHeight = 200.px
            padding = box(0.px, 0.px, 20.px, 0.px)
            borderInsets += box(0.px, 0.px, 20.px, 0.px)
            backgroundInsets += box(0.px, 0.px, 20.px, 0.px)
        }

        fieldset child label {
            fontSize = 1.5.em
            fontWeight = FontWeight.EXTRA_BOLD
        }
    }
}
