package com.grainger.keepstock.newsnow.views

import com.grainger.keepstock.newsnow.viewModels.MainViewModel
import javafx.application.Platform
import javafx.geometry.Pos
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import tornadofx.*

class MainView() : View("KeepStock ServiceNow App Request") {

    private lateinit var scroll: ScrollPane
    private lateinit var firstField: TextField
    private var resetSelected = false
    override val root = BorderPane()
    private val vm: MainViewModel by inject()

    init {
        restoreScreenPosition()
        with(root) {
            center {
                scroll = scrollpane {
                    vbox {

                        hbox {
                            label("API Key  ")
                            textfield(vm.apiKey) {
                                promptText = "Use `invoke api-key`"
                            }

                            alignment = Pos.BASELINE_RIGHT
                            button("Reset") {
                                hboxConstraints {
                                    margin = insets(10.0)
                                }
                                action {
                                    vm.invalidate()
                                    config.clear()
                                    config.save()
                                    resetSelected = true
                                }
                            }
                        }

                        hbarPolicy = ScrollPane.ScrollBarPolicy.NEVER
                        fitToWidthProperty().value = true
                        form {
                            fieldset("Create App") {
                                addClass(Styles.titleClass)

                                field("Entered By") {
                                    firstField = textfield(vm.entered_by)
                                }
                                field("Source of Change") {
                                    textfield(vm.source_of_change)
                                }
                                field("Configuration Item") {
                                    textfield(vm.configuration_item)
                                }
                                field("Standard Change") {
                                    textfield(vm.standard_change)
                                }
                                field("Short Description") {
                                    textarea(vm.short_description)
                                }
                                field("Description") {
                                    textarea(vm.description)
                                }
                                field("Remediation Failure Plan") {
                                    textarea(vm.remediation_failure_plan)
                                }
                                field("Test Plan") {
                                    textfield(vm.test_plan)
                                }
                                field("Pretest Plan") {
                                    textarea(vm.pretest_plan)
                                }
                                field("Posttest Plan") {
                                    textarea(vm.posttest_plan)
                                }
                                field("Implementation Plan") {
                                    textfield(vm.implementation_plan)
                                }
                                field("Reason For Change") {
                                    textfield(vm.reason_for_change)
                                }
                                field("Planned Start Date") {
                                    textfield(vm.planned_start_date)
                                }
                                field("Planned End Date") {
                                    textfield(vm.planned_end_date)
                                }
                                field("Contact") {
                                    textfield(vm.contact)
                                }
                                field("Contact Phone") {
                                    textfield(vm.contact_phone)
                                }
                                field("Country") {
                                    textfield(vm.country)
                                }
                                field("Production Target System") {
                                    textfield(vm.production_target_system)
                                }
                            }

                            fieldset("Create Tasks") {
                                addClass(Styles.titleClass)
                                var idx = 0
                                vm.tasks.forEach {
                                    fieldset("Task${idx++}") {
                                        field("assignment_group") {
                                            textfield(it.assignment_group)
                                        }
                                        field("description") {
                                            textarea(it.description)
                                        }
                                        field("short_description") {
                                            textarea(it.short_description)
                                        }
                                        field("start") {
                                            textfield(it.start)
                                        }
                                    }
                                }
                            }

                            fieldset("Status Messages") {
                                addClass(Styles.titleClass)
                                textarea(vm.statusMessages) {
                                    isEditable = false
                                }
                            }
                        }
                    }
                }
            }

            bottom {
                hbox {

                    alignment = Pos.BASELINE_RIGHT
                    button("Submit") {
                        hboxConstraints {
                            margin = insets(10.0)
                        }
                        enableWhen(vm.canSubmit)
                        action {
                            scroll.vvalue = scroll.vmax
                            vm.submit()
                        }
                    }
                    button("Cancel") {
                        hboxConstraints {
                            margin = insets(10.0)
                        }
                        action {
                            primaryStage.hide()
                        }
                    }
                }
            }
        }
    }

    private fun restoreScreenPosition() {
        if (config.any()) {
            with(config) {
                primaryStage.let {
                    it.x = string("screenX")?.toDouble() ?: it.x
                    it.y = string("screenY")?.toDouble() ?: it.y
                    it.width = string("screenWidth")?.toDouble() ?: it.width
                    it.height = string("screenHeight")?.toDouble() ?: it.height
                    it.isFullScreen = string("fullScreen")?.toBoolean() ?: it.isFullScreen
                    it.isMaximized = string("maximized")?.toBoolean() ?: it.isMaximized
                }
            }
        }

        primaryStage.setOnHiding {
            saveScreenPosition()
        }

        primaryStage.setOnShown {
            firstField.requestFocus()
        }
    }

    private fun saveScreenPosition() {
        if (!resetSelected) {
            vm.commit()
            with(config) {
                primaryStage.let {
                    put("screenX", it.x.toString())
                    put("screenY", it.y.toString())
                    put("screenWidth", it.width.toString())
                    put("screenHeight", it.height.toString())
                    put("fullScreen", it.isFullScreen.toString())
                    put("maximized", it.isMaximized.toString())
                }
            }
            config.save()
        }
    }
}
